from __future__ import annotations
import typing as ty
import functools
import itertools


T = ty.TypeVar("T")


def identity(x: T) -> T:
    return x


def delayed_partial(function: ty.Callable) -> ty.Callable:
    def get_arg(arg: ty.Any):
        return functools.partial(function, arg)

    return get_arg


## Functor ##
class Functor(ty.Protocol):
    def fmap(self, function: ty.Callable):
        pass


def fmap(function: ty.Callable, functor):
    return functor.fmap(function)


## Applicative##
class Applicative(Functor, ty.Protocol):
    def unit(self, var: ty.Any):
        pass

    def apply(self, other: Applicative):
        pass


class BaseApplicative:
    def unit(self, var):
        raise NotImplementedError

    def apply(self, other):
        raise NotImplementedError

    def fmap(self, func):
        return apply(unit(func, self), self)


def apply(left: Applicative, right: Applicative) -> Applicative:
    # print(f"{left=}, {right=}")
    left_mapped = fmap(delayed_partial, left)
    applied = left_mapped.apply(right)

    try:
        return fmap(lambda x: x(), applied)
    except TypeError:
        return applied


def unit(var, applicative: Applicative) -> Applicative:
    return applicative.unit(var)


def lift(function: ty.Callable, applicative, *applicatives):
    # print(f"{function=}, {applicative=}")
    left = unit(function, applicative)
    # print(f"{left=}")
    left = apply(left, applicative)
    # print(f"{left=}")

    for app in applicatives:
        left = apply(left, app)

    return left


def apply_all(*args):
    return lift(identity, *args)


## Maybe ##
class Maybe(ty.Generic[T]):
    def __init__(self, m: ty.Any) -> None:
        self.maybe = m

    def __repr__(self) -> str:
        return f"Maybe({self.maybe})"

    def unit(self, var: T) -> Maybe[T]:
        return Maybe(var)

    def fmap(self, func):
        if self.maybe is not None:
            return Maybe(func(self.maybe))
        else:
            return Maybe(None)

    def apply(self, other):
        if self.maybe is not None and other.maybe is not None:
            return Maybe(self.maybe(other.maybe))
        else:
            return Maybe(None)


## ZipList##
class ZipList(ty.Generic[T]):
    def __init__(self, lst: list[T]) -> None:
        self.lst = lst
        self.repeats = False

    def __repr__(self) -> str:
        return f"ZipList({self.lst})"

    def fmap(self, func):
        fmapped_lst = []
        for arg in self.lst:
            fmapped_lst.append(func(arg))

        fmapped = ZipList(fmapped_lst)
        fmapped.repeats = self.repeats

        return fmapped

    def unit(self, val: T) -> ZipList[T]:
        zl = ZipList([val])
        zl.repeats = True
        return zl

    def apply(self, other):
        applied = []
        if self.repeats:
            left = itertools.repeat(self.lst[0])
        else:
            left = self.lst

        for func, arg in zip(left, other.lst):
            applied.append(func(arg))

        right = ZipList(applied)
        right.repeats = other.repeats

        return right


if __name__ == "__main__":
    print("testing:")

    def my_times_2(x):
        return 2 * x

    def my_times_3(x):
        return 3 * x

    def my_add_operator(a, b):
        return a + b

    def my_times_operator(a, b):
        return a * b

    def my_add_troperator(a, b, c):
        return a + b + c

    def my_times_troperator(a, b, c):
        return a * b * c

    print("\nMaybe:")
    m_5: Maybe[ty.Optional[int]] = Maybe(5)
    m_8: Maybe[ty.Optional[int]] = Maybe(8)
    m_13: Maybe[ty.Optional[int]] = Maybe(13)
    m_nothing: Maybe[ty.Optional[int]] = Maybe(None)

    print(f"{fmap(lambda x: x + 1, m_5) = }")
    print(f"{fmap(lambda x: x + 1, m_nothing) = }")

    print(f"{lift(my_add_operator, m_5, m_8) = }")
    print(f"{lift(my_add_troperator, m_5, m_8) = }")
    print(f"{lift(my_add_operator, m_5, m_nothing) = }")
    print(f"{lift(my_add_troperator, m_5, m_nothing) = }")
    print(f"{lift(my_add_troperator, m_5, m_8, m_13) = }")
    print(f"{lift(my_add_troperator, m_5, m_nothing, m_8) = }")

    print("\nZipList")
    z_3v = ZipList([11, 22, 33])
    z_5v = ZipList([1, 2, 3, 4, 5])

    z_2times = ZipList([my_times_2, my_times_3])
    z_2op = ZipList([my_add_operator, my_times_operator])
    z_2trop = ZipList([my_add_troperator, my_times_troperator])

    print(f"{fmap(my_times_2, z_3v) = }")
    print(f"{apply(z_2times, z_3v) = }")
    print(f"{apply_all(z_2op, z_3v, z_5v) = }")
    print(f"{apply(z_2op, z_3v) = }")
